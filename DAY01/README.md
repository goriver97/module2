# sk infosec cloud ai 전문가 양성 과정

### 20/08/04

### 클라우드 


***
#### 
1. 4차 산업 혁명
    * I C B M -> A B C D
    * AI > 예측 & 추천 시스템 => 데이터 처리의 목적은 예측이다.
    * CLOUD > 데이터 저장 공간
    * POINT 는 위 모든 것들은 네트워크가 필수다.
        * HYPER CONNECTIVELY
    * __WHAT & HOW & INSIGHT__
        - 데이터 처리할때 알려주신 FLOW로 해야한다.
        - 활용할때 기획부터 생각해야한다.
    
    + __기획>수집>저장>처리>분석>시각화>활용__
           

2. 데이터 처리의 종류
    - 데이터 베이스의 목적 __1) 저장 2) 검색 3) 중복처리__
* RDMBS(관계형 데이터 베이스)
    - 1. 오라클
    - 2. MS SQL
    - 3. My SQL
    - 4. SYBASE
    - 5. DB2 (은행권)
    - 6. SQL LITE (모바일)
* NOSQL
    - 1. MONGO DB
    - 2. HADOOP = HBASE
    - 3. RE1+4


3. INTRO
    * 인공지능 적용 기술 
        - HADOOP, SPARK, 등등
    * 머신러닝/디벌닝 프로젝트 단계
        - INPUT으로 DATA와 결과값이 같이 들어간다.
            - 그러면 패턴을 찾아낸다.
    * 

***
***
#### 클라우드 컴퓨팅 basic

1. 클라우드 가상기술
    * 가상머신 : OS 중심
    * 컨테이너 : 이미지(DB 서버 서비스 중심)

    + __방법__
        - 1) 통합
            - EX) 그리딩 컴퓨팅
                - 슈퍼컴퓨터만큼의 성능이 나온다.
        - 2) 분할
            - EX) 서버를 10개로 나눠줌. OS 뿐만 아니라 CPU, MEMORY 등 다~ 가능하다!
    + __기술__
        - 1) CPU
        - 2) MEMORY -> 가장 먼저 가상화 기술을 적용한 분야
        - 3) STORY
        - 4) NETWORK
    + __Scale Out__, __Scale Up__
        - up> 이건 잘 안함
        - out> 박스를 여러개 만들어서 확장시켜줌(늘려줌)
            - 도커 컴포스, 쿠버네틱스 등에서 scale out에서 갯수를 정할 수 있다. 
            - ★용어 알아두기★

    * 가상화 기술의 핵심
        - 하이퍼바이저(virture box)와 가상화
        - pc에 리눅스 깔기(가상머신 올려서 설치)
        - 금요일에 openstack을 할 예정이다.  

***

##### principles of cloud computing
* 정의
    - 정의 : 클라우드 컴퓨팅은 __인터넷__ 기술을 이용하여 내 외부 고객들에게 __확장성(scalable)__ 있고 __탄력적인(elastic)__ it서비스가 제공되는 형식
    - cloud is a style of computing where scalable and elastic it-related capabilities are provided as a service to customer using internet technologies
    - __회복성__
    - 확장성(범위성)은 늘어나는 것만이 아니라 줄어드는 것도 포함
    - 컴퓨팅 자원이 필요한 만큼 늘어나거나 적정 수준으로 줄어들 수 있음
    - 이러한 작업이 수분에서 수십분 이내로 가능
    - 확장성은 범위, 탄력성은 시가
    - ★용어 알아두기- 확장성, 탄력성, 회복성, scale up, scale out★ 
    - <img src="/uploads/950ec59c2162630d5bc7f204b0667ee9/image.png" width = "60%"></img>
    - scale up/out 알아둬라

    ***
    * *최근 사람들이 클라우드로 하고 싶은 것*
    1. 자동화
    2. 관리(장애대응(복구), 보안)
        - 최근 관리를 __오케스트레이션__ 이라는 단어를 쓴다.
    3. 오토 -> ai로 어떠한 시간대에 빠른 곳이 어딘지 확인할 수 있다.
